package org.example;

public class Costumer {

   private double distance;

    public Costumer(double distance){
    this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Costumer{" +
                "distance=" + distance +
                '}';
    }
}
